package com.cidenet.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Lanzador de aplicacion
 * 
 * @author Carlos Lopez
 *
 */
@SpringBootApplication
public class BackEmpleadosApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackEmpleadosApplication.class, args);
	}

}
