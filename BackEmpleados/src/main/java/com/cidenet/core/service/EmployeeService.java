package com.cidenet.core.service;

import java.util.List;
import java.util.Optional;

import com.cidenet.core.entities.Employee;

/**
 * La interface no presta los servicios para ser creados s
 * 
 * @author Carlos Lopez
 *
 */
public interface EmployeeService {
	public List<Employee> findAllEmployees();

	public Optional<Employee> findEmployeeById(int id);

	public boolean saveEmployee(Employee EmployeeNew);

	public boolean deleteEmployee(int id);

	public boolean updateEmployee(Employee EmployeeNew);
}
