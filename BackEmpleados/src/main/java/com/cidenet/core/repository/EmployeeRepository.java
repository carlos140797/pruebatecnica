package com.cidenet.core.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cidenet.core.entities.Employee;

/**
 * 
 * @author Carlos Lopez
 *
 */
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
	Void save(Optional<Employee> customerToUpdate);
}