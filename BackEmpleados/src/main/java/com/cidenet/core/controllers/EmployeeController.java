package com.cidenet.core.controllers;

import java.util.List;
import java.util.Optional;

import com.cidenet.core.entities.Employee;

/**
 * Interface que controla el empleado, brinda las opciones de CRUD
 * 
 * @author Carlos Lopez
 *
 */
public interface EmployeeController {
	public List<Employee> getEmployees();

	public Optional<Employee> getEmployeeById(int id);

	public boolean addEmployee(Employee employee);

	public boolean deleteEmployee(int id);

	public boolean updateEmployee(Employee employeeNew);
}
