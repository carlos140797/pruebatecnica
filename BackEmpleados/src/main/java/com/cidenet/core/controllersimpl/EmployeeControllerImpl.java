package com.cidenet.core.controllersimpl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.cidenet.core.controllers.EmployeeController;
import com.cidenet.core.entities.Employee;
import com.cidenet.core.service.EmployeeService;

@CrossOrigin(maxAge = 3600)
@RestController
public class EmployeeControllerImpl implements EmployeeController {
	@Autowired
	EmployeeService employeeService;

	/**
	 * Metodo que lista todos los empleados desde la url, extrae toda la informacion
	 */
// http://localhost:8080/employees (GET)		
	@Override
	@RequestMapping(value = "/employee", method = RequestMethod.GET, produces = "application/json")
	public List<Employee> getEmployees() {
		return employeeService.findAllEmployees();
	}

	/**
	 * Metodo que extrae unicamente el Json que se le envia por Id
	 * 
	 * @param id codigo con el que se extrae la informacion
	 */
// http://localhost:8080/employees/1 (GET)		
	@Override
	@RequestMapping(value = "/employee/{id}", method = RequestMethod.GET, produces = "application/json")
	public Optional<Employee> getEmployeeById(@PathVariable int id) {
		return employeeService.findEmployeeById(id);
	}

	/**
	 * Metodo para adicionar un empleado nuevo
	 * 
	 * @param employee Empleado sin ID para ser añadido en la Lista
	 */
	// http://localhost:8080/employees/add (ADD)
	@Override
	@RequestMapping(value = "/employee/add", method = RequestMethod.POST, produces = "application/json")
	public boolean addEmployee(@RequestBody Employee employee) {
		employee = validateEmail(employee);
		employee.setUpdateAt(new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
		employee.setCreatedAt(new SimpleDateFormat("dd-MM-yyyy").format(new Date()));
		if (validateDocument(employee.getDocument())) {
			return false;
		}
		employeeService.saveEmployee(employee);
		return true;
		
	}

	/**
	 * Metodo para validar que un documento no este repetido en un empleado
	 * 
	 * @param employee
	 * @return existencia del documento
	 */
	private boolean validateDocument(String document) {
		List<Employee> validDocuments = getEmployees();
		for (Employee documents : validDocuments) {
			if (documents.getDocument().contains(document)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Metodo para eliminar empleados por el id
	 * 
	 * @param id es el objeto- registro que deseamos eliminar
	 */

	// http://localhost:8080/employees/delete/1 (GET)		
	@Override
	@RequestMapping(value = "/employee/delete/{id}", method = RequestMethod.DELETE)
	public boolean deleteEmployee(@PathVariable int id) {
		employeeService.deleteEmployee(id);
		return true;
	}

	/**
	 * Metodos para modificar el empleado lo envia al servicio
	 * 
	 * @param employeeNew el objeto que se reemplazara
	 */
	// http://localhost:8080/employees/update (PUT)		
	@Override
	@PatchMapping(value = "/employee/update/{id}")
	public boolean updateEmployee(@RequestBody Employee employeeNew) {
		employeeNew = validateEmail(employeeNew);
		return employeeService.updateEmployee(employeeNew);
	}

	/**
	 * Metodos para modificar o crear el email de un empleado
	 * 
	 * @param employee
	 * @return existencia del documento
	 */
	public Employee validateEmail(Employee employee) {
		List<Employee> validEmails = getEmployees();
		String email = employee.getFirstName() + "." + employee.getLastName() + "@cidenet.com.";
		if (employee.getCountry().equalsIgnoreCase("colombia")) {
			email += "co";
		} else {
			email += "us";
		}
		int index = 0;
		email = email.replace("\\s", "");
		email = email.toLowerCase();
		String emailaux = email;

		for (Employee mail : validEmails) {
			if (mail.getEmail().contains(email)) {
				email = emailaux.replace("@", "." + (++index) + "@");
			}
		}
		employee.setEmail(email);
		employee.setState("Activo");
		return employee;

	}

}