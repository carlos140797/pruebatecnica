package com.cidenet.core.serviceImpl;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.cidenet.core.entities.Employee;
import com.cidenet.core.repository.EmployeeRepository;
import com.cidenet.core.service.EmployeeService;

/**
 * Implementador del servicio
 * 
 * @author Carlos Lopez
 *
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {
	@Autowired
	EmployeeRepository employeeRepository;

	@CrossOrigin(maxAge = 3600)
	@Override
	public List<Employee> findAllEmployees() {
		return employeeRepository.findAll();
	}

	@Override
	public Optional<Employee> findEmployeeById(int id) {
		Optional<Employee> employee = employeeRepository.findById(id);
		return employee;
	}
	
	/**
	 * Metodo para guardar un empleado 
	 */

	@Override
	public boolean saveEmployee(Employee employeeNew) {

		if (employeeNew != null) {
			employeeNew.setEmail(validateEmail(employeeNew));
			employeeNew.setCreatedAt(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
			employeeNew.setUpdateAt(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
			employeeRepository.save(employeeNew);
			return true;
		}
		return false;
	}

	/**
	 * Metodo para eliminar el empleado
	 */
	@Override
	public boolean deleteEmployee(int id) {
		if (employeeRepository.findById(id).isPresent()) {
			employeeRepository.deleteById(id);
			return true;
		}
		return false;
	}
	/**
	 * Metodo para validar que un documento no este repetido en un empleado
	 * 
	 * @param employee
	 * @return existencia del documento
	 */
	private boolean validateDocument(String document) {
		List<Employee> validDocuments = findAllEmployees();
		for (Employee documents : validDocuments) {
			if (documents.getDocument().equalsIgnoreCase(document)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Servicio que hace update del empleado
	 */
	@Override
	public boolean updateEmployee(Employee employeeUpdated) {
		int num = employeeUpdated.getId();
		Optional<Employee> actual = employeeRepository.findById(num);
		Employee employeeToUpdate = new Employee();
		if (employeeRepository.findById(num).isPresent()) {
			if (actual.get().getFirstName().equalsIgnoreCase(employeeUpdated.getFirstName())
					&& actual.get().getLastName().equalsIgnoreCase(employeeUpdated.getLastName())) {
				employeeToUpdate.setEmail(actual.get().getEmail());
			} else {
				employeeToUpdate.setEmail(validateEmail(employeeUpdated));
			}
			employeeToUpdate.setDocument(employeeUpdated.getDocument());
			if (!actual.get().getDocument().equals(employeeToUpdate.getDocument())) {
				if (validateDocument(employeeToUpdate.getDocument())) {
					return false;
				}
			}
			employeeToUpdate.setCreatedAt(actual.get().getCreatedAt());
			employeeToUpdate.setAddmissionDate(employeeUpdated.getAddmissionDate());
			employeeToUpdate.setId(employeeUpdated.getId());
			employeeToUpdate.setFirstName(employeeUpdated.getFirstName());
			employeeToUpdate.setMiddleName(employeeUpdated.getMiddleName());
			employeeToUpdate.setLastName(employeeUpdated.getLastName());
			employeeToUpdate.setSecondLastName(employeeUpdated.getSecondLastName());
			employeeToUpdate.setCountry(employeeUpdated.getCountry());
			employeeToUpdate.setDocumentType(employeeUpdated.getDocumentType());
			
			employeeToUpdate.setArea(employeeUpdated.getArea());
			employeeToUpdate.setState(employeeUpdated.getState());
			employeeToUpdate.setUpdateAt(new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").format(new Date()));
			employeeRepository.save(employeeToUpdate);
			return true;
		}
		return false;
	}

	/**
	 * Validacion del email en caso de que el usuario haya cambiado de nombre y apellido
	 * @param employee
	 * @return
	 */
	public String validateEmail(Employee employee) {
		List<Employee> validEmails = findAllEmployees();
		String email = employee.getFirstName() + "." + employee.getLastName() + "@cidenet.com.";
		email = email.replaceAll(" ", "");
		if (employee.getCountry().equalsIgnoreCase("colombia")) {
			email += "co";
		} else {
			email += "us";
		}
		int index = 0;
		email = email.toLowerCase();
		String emailaux = email;

		for (Employee mail : validEmails) {
			if (mail.getEmail().contains(email)) {
				email = emailaux.replace("@", "." + (++index) + "@");
			}
		}
		email = email.replaceAll(" ", "");
		employee.setEmail(email);
		employee.setState("Activo");
		return employee.getEmail();

	}
}